import { Route } from '@angular/router';

import { NavbarComponent } from './layouts';
import { UserRouteAccessService } from './shared';

export const navbarRoute: Route = {
    path: '',
    component: NavbarComponent,
    canActivate: [UserRouteAccessService],
    outlet: 'navbar'
};
