import { Route, Routes } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { HomeComponent } from './';
import { ALL_AUTHORITY } from '../shared/auth/authorities.constants';

export const HOME_ROUTE: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent,
        data: {
            authorities: ALL_AUTHORITY,
            pageTitle: 'home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
