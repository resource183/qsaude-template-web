import './vendor.ts';

import { LOCALE_ID, NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './material.module';
import { Ng2Webstorage } from 'ngx-webstorage';

import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { QSaudeSeuModuloWebSharedModule, UserRouteAccessService } from './shared';
import { QSaudeSeuModuloWebAppRoutingModule} from './app-routing.module';
import { QSaudeSeuModuloWebHomeModule } from './home/home.module';
import { QSaudeSeuModuloWebAdminModule } from './admin/admin.module';
import { QSaudeSeuModuloWebEntityModule } from './entities/entity.module';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

import { SidebarService } from './layouts/side-navbar/side-bar-service';
import { SideNavbarItemComponent } from './layouts/side-navbar-item/side-navbar-item.component';
import { SideNavbarComponent  } from './layouts/side-navbar/side-navbar.component';
import { QSaudeSeuModuloWebGoTopButtonModule } from './layouts/back-to-top/go-top-button.module';
import { QSaudeWebOperadoraModule } from './layouts/operadora/operadora.module';

import { LocalStorageService } from 'ng2-webstorage';
import { JhiTrackerService } from './shared/tracker/tracker.service';
import { WindowRef } from './shared/tracker/window.service';
import { QSaudeWebLoginModule } from './shared/login';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import localePTBR from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { QSaudeSeuModuloAccountModule } from './account/account.module';

import { SidebarModule } from 'primeng/sidebar';

import { GrowlModule } from 'primeng/components/growl/growl';

registerLocaleData(localePTBR);

@NgModule({
    imports: [
        BrowserModule,
        QSaudeWebLoginModule,
        QSaudeSeuModuloWebAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        QSaudeSeuModuloWebSharedModule,
        QSaudeSeuModuloWebHomeModule,
        QSaudeSeuModuloWebAdminModule,
        QSaudeSeuModuloWebEntityModule,
        QSaudeWebOperadoraModule,
        QSaudeSeuModuloAccountModule,
        BrowserAnimationsModule,
        QSaudeSeuModuloWebGoTopButtonModule,
        SidebarModule,
        GrowlModule
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent,
        SideNavbarItemComponent,
        SideNavbarComponent

    ],
    providers: [
        JhiTrackerService,
        WindowRef,
        LocalStorageService,
        NgbActiveModal,
        ProfileService,
        PaginationConfig,
        UserRouteAccessService,
        SidebarService,

        {provide: LOCALE_ID, useValue: 'pt-BR'}
    ],
    bootstrap: [ JhiMainComponent ]
})
export class QSaudeSeuModuloWebAppModule {}
