export class ErrorMsg {
    constructor(public header?: string,
                public entityKey?: string) {
    }
}

export class ErrorUtils {
    public static readErrorHeader(httpResponse): ErrorMsg {
        const error = new ErrorMsg();
        const arr = Array.from(httpResponse.headers._headers);
        const headers = [];
        for (let i = 0; i < arr.length; i++) {
            if (arr[i][0].endsWith('app-error') || arr[i][0].endsWith('app-params')) {
                headers.push(arr[i][0]);
            }
        }
        headers.sort();
        if (headers.length > 1) {
            error.header = httpResponse.headers.get(headers[0]);
            error.entityKey = httpResponse.headers.get(headers[1]);
        }
        return error;
    }
}