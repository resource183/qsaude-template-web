import { NgModule, LOCALE_ID } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { registerLocaleData } from '@angular/common';
// import locale from '@angular/common/locales/pt';

import {
    QSaudeSeuModuloWebSharedLibsModule,
    JhiLanguageHelper,
    FindLanguageFromKeyPipe,
    JhiAlertComponent,
    JhiAlertErrorComponent
} from './';
import { WindowRef } from './tracker/window.service';
import { JhiGrowComponent } from './alert/growl.component';
import { JhiGrowErrorComponent } from './alert/growl-error.component';

@NgModule({
    imports: [
        QSaudeSeuModuloWebSharedLibsModule
    ],
    declarations: [
        FindLanguageFromKeyPipe,
        JhiAlertComponent,
        JhiAlertErrorComponent,
        JhiGrowComponent,
        JhiGrowErrorComponent
    ],
    providers: [
        JhiLanguageHelper,
        WindowRef,
        Title,
        {
            provide: LOCALE_ID,
            useValue: 'pt-br'
        },
    ],
    exports: [
        QSaudeSeuModuloWebSharedLibsModule,
        FindLanguageFromKeyPipe,
        JhiAlertComponent,
        JhiAlertErrorComponent,
        JhiGrowComponent,
        JhiGrowErrorComponent
    ]
})
export class QSaudeSeuModuloWebSharedCommonModule {
    /*constructor() {
        registerLocaleData(locale);
    }*/
}
