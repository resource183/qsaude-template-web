import {Component, DoCheck, IterableDiffers, OnInit} from '@angular/core';
import {JhiAlert, JhiAlertService} from 'ng-jhipster';
import {Message} from 'primeng/components/common/message';

@Component({
    selector: 'jhi-growl',
    template: '<p-growl [(value)]="msgs" [immutable]="false"></p-growl>'
})
export class JhiGrowComponent implements OnInit, DoCheck {
    msgs: Message[] = [];
    iterableDiffer: any;
    last: any;

    constructor(private alertService: JhiAlertService,
                private _iterableDiffers: IterableDiffers) {
        this.msgs = [];
        this.iterableDiffer = this._iterableDiffers.find([]).create(null);
    }

    ngOnInit() {
    }

    private convert(jhiAlerts: JhiAlert[]) {
        if (jhiAlerts.length > 0 && jhiAlerts[jhiAlerts.length - 1] !== this.last) {
            const alert = jhiAlerts[jhiAlerts.length - 1];
            this.msgs.push({
                severity: alert.type === 'danger' ? 'error' : alert.type === 'warning' ? 'warn' : alert.type,
                detail: alert.msg
            });
            this.last = alert;
        }
    }

    ngDoCheck() {
        const changes = this.iterableDiffer.diff(this.alertService.get());
        if (changes) {
            this.convert(this.alertService.get());
        }
    }
}
