import {Component, DoCheck, IterableDiffers, OnDestroy, OnInit} from '@angular/core';
import {JhiAlert, JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Message} from 'primeng/components/common/message';
import {Subscription} from 'rxjs/Rx';
import {TranslateService} from '@ngx-translate/core';
import { ErrorUtils } from '../util/errorUtils';

@Component({
    selector: 'jhi-growl-error',
    template: '<p-growl [(value)]="alerts" [immutable]="false"></p-growl>'
})
export class JhiGrowErrorComponent implements OnDestroy {
    alerts: Message[];
    cleanHttpErrorListener: Subscription;

    constructor(private alertService: JhiAlertService, private eventManager: JhiEventManager, private translateService: TranslateService) {
        this.alerts = [];

        this.cleanHttpErrorListener = eventManager.subscribe('qSaudeSeuModuloWebApp.httpError', (response) => {
            let i;
            const httpResponse = response.content;
            switch (httpResponse.status) {
                // connection refused, server not reachable
                case 0:
                    this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;

                case 400:
                    const error = ErrorUtils.readErrorHeader(httpResponse);
                    if (error.header) {
                        const entityName = translateService.instant('global.menu.entities.' + error.entityKey);
                        this.addErrorAlert(error.header, error.header, {entityName});
                    } else if (httpResponse.text() !== '' && httpResponse.json() && httpResponse.json().fieldErrors) {
                        const fieldErrors = httpResponse.json().fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            const fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            const convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            const fieldName = translateService.instant('qSaudeSeuModuloWebApp.' +
                                fieldError.objectName + '.' + convertedField);
                            this.addErrorAlert(
                                'Error on field "' + fieldName + '"', 'error.' + fieldError.message, {fieldName});
                        }
                    } else if (httpResponse.text() !== '' && httpResponse.json() && httpResponse.json().message) {
                        this.addErrorAlert(httpResponse.json().message, httpResponse.json().message, httpResponse.json().params);
                    } else {
                        this.addErrorAlert(httpResponse.text());
                    }
                    break;

                case 404:
                    this.addErrorAlert('Not found', 'error.url.not.found');
                    break;

                default:
                    if (httpResponse.text() !== '' && httpResponse.json() && httpResponse.json().message) {
                        this.addErrorAlert(httpResponse.json().message);
                    } else {
                        this.addErrorAlert(httpResponse.text());
                    }
            }
        });
    }

    ngOnDestroy() {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
            this.alerts = [];
        }
    }

    addErrorAlert(message, key?, data?) {
        key = (key && key !== null) ? key : message;
        this.alerts.push(
            {
                severity: 'error',
                detail: key ? this.translateService.instant(key, data) : message
            }
        );
    }
}
