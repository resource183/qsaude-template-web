import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { DominioEnum } from '../enum/dominio.enum';
import { DominioModel } from './dominio.model';

@Injectable()
export class DominioService {

  private resourceUrl = 'qsaudeoperadora/api/dominio-itens';

  constructor(private http: Http) { }

  recuperarDominio(dominioChave: DominioEnum): Observable<DominioModel[]> {
    return this.http.get(`${this.resourceUrl}/${dominioChave}`).map((res: Response) => {
      return this.converterObjDominio(res.json());
    });
  }

  private converterObjDominio(data): DominioModel[] {
    let dominios: DominioModel[] = new Array<DominioModel>();

    data.forEach(item => {
      dominios.push(
        {
          id: item.id,
          nome: item.nomeItem,
          descricao: item.descricaoItem
        }
      );
    });

    return dominios;
  }

}
