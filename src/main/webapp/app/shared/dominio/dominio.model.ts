export class DominioModel {

    constructor(
        public id?: number,
        public nome?: string,
        public descricao?: string
    ) {}
}
