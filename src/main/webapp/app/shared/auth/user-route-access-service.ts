import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Principal } from '../';
import { LoginModalService } from '../login/login-modal.service';
import { StateStorageService } from './state-storage.service';

import { URL_PORTAL_WEB, ABRIR_LOGIN_SISTEMA } from '../../app.constants';

@Injectable()
export class UserRouteAccessService implements CanActivate {

    constructor(private router: Router,
                private loginModalService: LoginModalService,
                private principal: Principal,
                private stateStorageService: StateStorageService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {
        const authorities = route.data['authorities'];
        
        return this.checkLogin(authorities, state.url);
    }

    checkLogin(authorities: string[], url: string): Promise<boolean> {
        const principal = this.principal;
        return Promise.resolve(principal.identity().then((account) => {

            if (!authorities || authorities.length === 0) {
                return true;
            }

            if (account) {
                return principal.hasAnyAuthority(authorities).then((response) => {
                    if (response) {
                        return true;
                    }
                    return false;
                });
            }

            this.stateStorageService.storeUrl(url);
            
            if (!account) {
                /* Para teste direto do módulo passar o parametro true */
                this.abrirLoginSistema(ABRIR_LOGIN_SISTEMA === 'true' ? true : false);
            }
            return false;
        }));
    }

    abrirLoginSistema(flagLoginLocal: boolean) {
        if (flagLoginLocal) {
            this.loginModalService.open();
        } else {
            location.href = URL_PORTAL_WEB;
        }
    }
}
