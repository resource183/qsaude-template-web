import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { IndiceEconomicoModel } from './indice-economico.model';

@Injectable()
export class IndiceEconomicoService {

  private resourceUrl = 'qsaudeoperadora/api/indice-economicos';

  constructor(private http: Http) { }

  listarIndiceEconomico(): Observable<IndiceEconomicoModel[]> {
    return this.http.get(this.resourceUrl).map((res: Response) => {
      return this.converterObjIndiceEconimico(res.json());
    });
  }

  private converterObjIndiceEconimico(data): IndiceEconomicoModel[] {
    let indiceEcon: IndiceEconomicoModel[] = new Array<IndiceEconomicoModel>();

    data.forEach(indice => {
      indiceEcon.push(
        {
          id: indice.id,
          codigoIndiceEconomico: indice.codigoIndiceEconomico,
          nomeIndiceEconomico: indice.nomeIndiceEconomico,
          siglaIndiceEconomico: indice.siglaIndiceEconomico
        }
      );
    });

    return indiceEcon;
  }

}
