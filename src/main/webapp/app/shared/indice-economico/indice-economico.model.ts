export class IndiceEconomicoModel {
    constructor(
        public id?: number,
        public codigoIndiceEconomico?: number,
        public nomeIndiceEconomico?: string,
        public siglaIndiceEconomico?: string
    ) {}
}
