import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    QSaudeSeuModuloWebSharedLibsModule,
    QSaudeSeuModuloWebSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    Principal,
    HasAnyAuthorityDirective
} from './';
import { DominioService } from './dominio/dominio.service';
import { IndiceEconomicoService } from './indice-economico/indice-economico.service';

@NgModule({
    imports: [
        QSaudeSeuModuloWebSharedLibsModule,
        QSaudeSeuModuloWebSharedCommonModule
    ],
    declarations: [
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe,
        DominioService,
        IndiceEconomicoService
    ],
    entryComponents: [],
    exports: [
        QSaudeSeuModuloWebSharedCommonModule,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class QSaudeSeuModuloWebSharedModule {}
