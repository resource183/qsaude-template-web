import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LOGIN_ROUTE } from './';
import {LoginService} from './login.service';
import { QSaudeSeuModuloWebSharedModule, JhiLoginModalComponent } from '..';

@NgModule({
    imports: [
        QSaudeSeuModuloWebSharedModule,
        RouterModule.forRoot([ LOGIN_ROUTE ], { useHash: true })
    ],
    declarations: [
        JhiLoginModalComponent,
    ],
    entryComponents: [
    ],
    providers: [
        LoginService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QSaudeWebLoginModule {}
