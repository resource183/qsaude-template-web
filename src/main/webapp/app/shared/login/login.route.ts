import { Route } from '@angular/router';

import { JhiLoginModalComponent, UserRouteAccessService } from '../';

export const LOGIN_ROUTE: Route = {
    path: 'login',
    component: JhiLoginModalComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
