﻿import { Component } from '@angular/core';
import { SidebarService } from './side-bar-service';
import { SideNavbarItemModel } from '../side-navbar-item/side-navbar-item.model';
import { Principal } from '../../shared/auth/principal.service';
import { Modulos } from '../../shared/enum/modulos.enum';

@Component({
    selector: 'jhi-side-navbar',
    templateUrl: './side-navbar.component.html',
    styleUrls: ['side-navbar.scss']
})
export class SideNavbarComponent {
    showMenu = '';
    items: SideNavbarItemModel[];

    public Modulos = Modulos;

    constructor(public sidebarService: SidebarService,
        private principal: Principal) {
        this.attachEvents();
        
        /* MENU referente as funcionalidades do modulo */
        this.items = [
            {
                caminho: '',
                nome: 'SEUITEMMENU',
                nomeTradutor: 'global.menu.entities.SEUITEMMENU',
                icon: 'fa-id-card-o',
                modulo: [Modulos.SeuModulo],
                role: ['ROLE_ADMIN', 'ROLE_USER'],
                lista: []
            }
        ];
    }

    attachEvents() {
        this.sidebarService.onResize();
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    getModulo() {
        return this.principal.getModulo();
    }
}
