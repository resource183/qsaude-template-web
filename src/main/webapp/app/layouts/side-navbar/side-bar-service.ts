import {Injectable} from '@angular/core';

@Injectable()
export class SidebarService {
    toggle = true;
    mobileView = 992;

    toggleSidebar() {
        this.toggle = !this.toggle;
        localStorage.setItem('toggle', this.toggle.toString());
    }

    onResize() {
        window.onresize = () => {
            if (this.getWidth() >= this.mobileView) {
                if (localStorage.getItem('toggle')) {
                    this.toggle = localStorage.getItem('toggle') === 'true';
                } else {
                    this.toggle = true;
                }
            } else {
                this.toggle = false;
            }
        };
    }

    getWidth() {
        return window.innerWidth;
    }
}
