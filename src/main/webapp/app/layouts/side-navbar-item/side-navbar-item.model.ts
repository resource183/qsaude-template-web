﻿import { Modulos } from '../../shared/enum/modulos.enum';

export class SideNavbarItemModel {
    constructor(public caminho?: string,
                public nome?: string,
                public nomeTradutor?: string,
                public icon?: string,
                public role?: string[],
                public modulo?: Modulos[],
                public lista?: SideNavbarItemModel[]) {
    }
}
