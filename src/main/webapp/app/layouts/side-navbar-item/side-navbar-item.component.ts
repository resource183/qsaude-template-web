﻿import {Component, Input, ViewEncapsulation} from '@angular/core';
import { SideNavbarItemModel } from './side-navbar-item.model';
import { Modulos } from '../../shared/enum/modulos.enum';
import { Principal } from '../../shared/auth/principal.service';

@Component({
    selector: 'jhi-side-navbar-item',
    templateUrl: './side-navbar-item.component.html',
    styleUrls: ['../side-navbar-item/side-navbar-item.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SideNavbarItemComponent {
    @Input() item: SideNavbarItemModel;
    @Input() menuRoot = false;
    showMenu = '';

    constructor(private principal: Principal) {
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    getModulo() {
        return this.principal.getModulo();
    }
}
