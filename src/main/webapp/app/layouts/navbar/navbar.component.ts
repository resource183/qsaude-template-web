import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';

import { ProfileService } from '../profiles/profile.service';
import { Account, JhiLanguageHelper, Principal, LoginModalService, LoginService } from '../../shared';

import { VERSION, URL_PORTAL_WEB } from '../../app.constants';
import { Modulos } from '../../shared/enum/modulos.enum';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [
        'navbar.scss'
    ]
})
export class NavbarComponent implements OnInit {

    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    account: Account;

    urlModulos: string;
    flagModulos: boolean;

    urlPortalWeb;

    public Modulos = Modulos;

    constructor(
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private principal: Principal,
        private profileService: ProfileService,
        private router: Router,
        private loginService: LoginService,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private sanitizer: DomSanitizer
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }

    ngOnInit() {
        this.urlPortalWeb = this.sanitizer.bypassSecurityTrustResourceUrl( `${URL_PORTAL_WEB}modulo-iframe` );

        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        this.profileService.getProfileInfo().subscribe((profileInfo) => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
        this.registerAuthenticationSuccess();

        this.principal.setModulo(Modulos.SeuModulo);
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    changeLanguage(languageKey: string) {
      this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.loginService.logout();
        this.collapseNavbar();
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }

    setModulo(modulo: Modulos, pagina: string) {
        this.principal.setModulo(modulo);
        this.router.navigate([pagina]);
    }

    getModulo() {
        return this.principal.getModulo();
    }

    irPortal() {
        location.href = URL_PORTAL_WEB;
    }
}
