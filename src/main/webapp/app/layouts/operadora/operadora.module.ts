import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {operadoraRoute} from './';
import {DropdownModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {OperadoraComponent} from './operadora.component';
import {BrowserModule} from '@angular/platform-browser';
import {OperadoraService} from './operadora.service';

const ENTITY_STATES = [...operadoraRoute];

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        FormsModule,
        DropdownModule
    ],
    declarations: [
        OperadoraComponent
    ],
    exports: [
        OperadoraComponent
    ],
    entryComponents: [],
    providers: [
        OperadoraService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QSaudeWebOperadoraModule {
}
