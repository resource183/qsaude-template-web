import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Operadora } from './operadora.model';
import { createRequestOption } from '../../shared';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';

@Injectable()
export class OperadoraService {

    private resourceUrl = 'qsaudegamauaa/api/operadoras';

    constructor(private http: Http) { }

    find(id: number): Observable<Operadora> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(operadora: Operadora): Operadora {
        const copy: Operadora = Object.assign({}, operadora);
        return copy;
    }
}
