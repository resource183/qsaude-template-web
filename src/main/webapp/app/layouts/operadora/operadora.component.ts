import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Operadora } from './operadora.model';
import { OperadoraService } from './operadora.service';
import { Principal } from '../../shared';
import { SelectItem } from 'primeng/primeng';
import { ResponseWrapper } from '../../shared/model/response-wrapper.model';

@Component({
    selector: 'jhi-operadora',
    templateUrl: './operadora.component.html',
    styleUrls: ['operadora.scss']
})
export class OperadoraComponent implements OnInit {

    currentAccount: any;
    selectOperadoras: SelectItem[];
    operadoras: Operadora[];
    error: any;
    success: any;
    routeData: any;
    opSelecionada: any;

    constructor(
        private operadoraService: OperadoraService,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute
    ) {
        this.routeData = this.activatedRoute.data.subscribe();
        this.selectOperadoras = [];
        this.operadoras = [];

    }

    loadAll() {
        this.operadoraService.query().subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.operadoras = account.operadoras;
            this.opSelecionada = this.operadoras[0];
            this.converter(this.operadoras);
        });
    }

    private onSuccess(data, headers) {
        this.converter(data);
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    private converter(data: any) {
        data.forEach((operadora) => {
            this.selectOperadoras.push({ label: operadora.descricao, value: operadora });
        });
    }
}
