import { BaseEntity } from '../../shared';

export class Operadora implements BaseEntity {
    constructor(
        public id?: number,
        public descricao?: string,
        public logo?: string,
    ) {
    }
}
