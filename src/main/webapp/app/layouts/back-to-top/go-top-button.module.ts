import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {GoTopButtonComponent} from './go-top-button.component';
import {QSaudeSeuModuloWebSharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [
        QSaudeSeuModuloWebSharedModule
    ],
    declarations: [
        GoTopButtonComponent
    ],
    exports: [
        GoTopButtonComponent
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QSaudeSeuModuloWebGoTopButtonModule {
}
