/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.gamasaude.qsaude.seumodulo.web.rest.vm;
